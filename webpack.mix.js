const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .styles([
        'public/theme/css/bootstrap.min.css',
        'public/theme/css/themify-icons.css',
        'public/theme/css/owl.carousel.min.css',
        'public/theme/css/style.css',
        'public/theme/css/custom-theme.css'
    ], 'public/css/app-custom.css');

   
