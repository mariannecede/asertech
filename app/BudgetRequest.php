<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BudgetRequest extends Model
{
    protected $table = 'budgets_requests';
   
    protected $fillable = [
        'name',
        'telephone',     
        'email', 
        'product',
        'description'
    ];

    protected $hidden = [
        'id'
    ];
}
