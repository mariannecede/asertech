<?php

namespace App\Src\repositories;
use App\Subscriber;

class FindEmailSubscriberRepository
{
    public $subscriber;
 

    public function execute($command,$messaje)
    {
        $subscriber = Subscriber::where('email', $command)->get();
        return $this->isEmpty($subscriber,$messaje);

    }
    private function isEmpty($subscriber,$messaje)
    {
        if($subscriber->isEmpty()) {
            $this->createSubscriber();
        }
        $this->existSubscriber($messaje);
    }
   
    private function existSubscriber($messaje)
    {
        return $messaje; 
    }
   
    private function createSubscriber()
    {
       return redirect()->action('SubscriberController@store');
 
    }     
}