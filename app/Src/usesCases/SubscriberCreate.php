<?php
namespace App\Src\usesCases;

use App\Subscriber;


class SubscriberCreate 
{
    public $subscriber;
 

    public function execute($command)
    {
       return Subscriber::create($command);
    }
    
     

}
 