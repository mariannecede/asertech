<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailController extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($command)
    {
        $this->_from = $command['from'];
        $this->subject = $command['subject'];
        $this->view = $command['view'];
        $this->data = $command['data'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->_from)
                    ->subject($this->subject)
                    ->view($this->view)->with([
                        'data'=>$this->data
                    ]);
    }
}
