<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Src\usesCases\BudgetRequestCreate;
use App\Src\repositories\FindEmailSubscriberRepository;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use App\Mail\MailController;
use Illuminate\Support\Facades\Mail;


class BudgetRequestController
{
    public function __construct()
  {
    $this->budgetRequestCreate = new BudgetRequestCreate(); 
  }
  public function store(Request $request)
  { 
    $command = ['name'=>$request->get('name'),
                'telephone'=>$request->get('telephone'),
                'email'=>$request->get('email'),
                'product'=>$request->get('product'),
                'description'=>$request->get('description'),
            ];
   
    $budgetRequest = $this->budgetRequestCreate->execute($command); 

    Mail::to('cotizacion@asertech.io')->send(new MailController([
      'from'=>'asesoria@asertech.io',
      'subject'=> 'Solicitud de Presupuesto',
      'view'=>'send-budget-request-operator',
      'data'=>$budgetRequest
    ]));   
  
    return view('pages-send-form/send-budgetRequest');
        
  }
  
  public function show()
  {

  }
  
}