<?php

namespace App\Http\Controllers;

use App\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Src\usesCases\SubscriberCreate;
use App\Src\repositories\FindEmailSubscriberRepository;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use App\Mail\MailController;
use Illuminate\Support\Facades\Mail;


//use App\Http\Requests\SubscriberCreateValidator;


class SubscriberController extends BaseController
{
  public function __construct()
  {
    $this->subscriberCreate = new SubscriberCreate(); 
    $this->findEmailRepository = new FindEmailSubscriberRepository();
  }

  public function store(Request $request)
  { 
     $command = ['name'=>$request->get('name'),
                'email'=>$request->get('email'),
                'send_email'=>false,
                'exported'=>false ];
   
    $subscriber = $this->subscriberCreate->execute($command); 
   
    // Mail::to($command['email'])->send(new MailController());   
   
    return view('send-budgetRequest');
        
  }
  
  public function storeWeb(Request $request)
  { 
     $command = ['name'=>$request->get('name'),
                'email'=>$request->get('email'),
                'send_email'=>false,
                'exported'=>false ];
   
    $subscriber = $this->subscriberCreate->execute($command); 
   
    Mail::to('asesoria@asertech.io')->send(new MailController([
      'from'=>'asesoria@asertech.io',
      'subject'=> 'Solicitud de Asesoria',
      'view'=>'request-advisory',
      'data'=>$subscriber
    ]));   
   
    return view('send-budgetRequest');
  }

  
  public function show($email)
  {
    $command = $email;
    $message = "usted ya esta participando";
    $subscriber= $this->findEmailRepository->execute($command,$message);
      
    return $subscriber;
  }
  

  


}
