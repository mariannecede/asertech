export default {
    create(params, success, error) {
        let url = "api/subscriber";
        return axios.post(url, params, success, error);
    },

    find(params, success, error) {
        let url = "api/subscriber";
        return axios.get(url, params, success, error);
    }
}