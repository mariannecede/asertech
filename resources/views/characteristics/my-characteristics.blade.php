
<div class="container">

    <div class="section-title">
        <h3>CARACTERÍSTICAS QUE NOS DEFINEN</h3>
    </div>

    <div class="row">
        <div class="col-12 col-lg-4">
            <div class="card features">
                <div class="card-body">
                    <div class="media">
                        <span class="ti-light-bulb gradient-fill ti-3x mr-3"></span>
                        <div class="media-body">
                            <h4 class="card-title">Ideamos</h4>
                            <p class="card-text"> Estrategias centradas en obtención de datos, 
                                para dar paso a soluciones adaptadas a tu medida.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-4">
            <div class="card features">
                <div class="card-body">
                    <div class="media">
                        <span class="ti-thought gradient-fill ti-3x mr-3"></span>
                        <div class="media-body">
                            <h4 class="card-title">Desarrollamos</h4>
                            <p class="card-text"> Soluciones con altos stándares técnicos,
                                respaldado de un equipo altamente calificado para atender sus exigencias. 
                                     </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-4">
            <div class="card features">
                <div class="card-body">
                    <div class="media">
                        <span class="ti-stats-up gradient-fill ti-3x mr-3"></span>
                        <div class="media-body">
                            <h4 class="card-title">Impulsamos</h4>
                            <p class="card-text">Tu imagen corporativa en todos los medios 
                                digitales, que complementen el flujo operativo de tu negocio. 
                                  </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
