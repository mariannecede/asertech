<div class="container">
		<div class="section-title">
				<h3>Cotización</h3>
			</div>
    <form action="{{action('BudgetRequestController@store')}}" method="POST">	
		    @csrf

		<div class="row">
			<div class="col-md-6">

					<div class="form-group">
							<label for="name">Nombre y Apellido</label>
							<input type="text" class="form-control input100" name="name" placeholder="Nombre y Apellido" required>
							<div class="valid-feedback">
									Excelente!
							</div>
					</div> 
					<div class="form-group">
							<label for="numero">Teléfono</label>
							<input type="telephone" class="form-control input100" name="telephone" placeholder="Teléfono" required>
							<div class="valid-feedback">
									Excelente!
										</div>
					</div> 
					<div class="form-group">
							<label for=email>Email</label>
							<input type="email" class="form-control input100" name="email" placeholer="Email" placeholder="Correo eléctronico" required>
							<div class="valid-feedback">
									Excelente!
							</div>
					</div>

			</div>
			<div class="col-md-6">
					                  
					<div class="form-group">
							<label for="">Producto</label>
							<select class="form-control input100 input100-decorator" name="product" id="product" required>
							  <option value="1">Página web</option>
							  <option value="2">Aplicación</option>
							  <option value="3">Branding</option>
							  <option value="4">Redes Sociales</option>
							  <option value="5">Otro</option>
							 </select>
						</div>
						   <div class="form-group">
												  <label for="description">Descripción</label>
												  <textarea name="description" class="form-control input100 input100-decorator p-t-12" rows="4" required></textarea>
												  <div class="valid-feedback">
																  Excelente!
							</div>
						</div>
									 
					  <button class="btn btn-primary button-form-budget" type="submit">Enviar</button>
			  
		  
			</div>
		</div>
     </form>	   
		          
</div>

<script>
        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function() {
          'use strict';
          window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
              form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                  event.preventDefault();
                  event.stopPropagation();
                }
                form.classList.add('was-validated');
              }, false);
            });
          }, false);
        })();
 </script> 

 
