     
 <div class="container">
               
    <div class="row">
     	    <div class="col-md-6">
			    <div class="login100-pic js-tilt" data-tilt>
				   <img src="{{asset('theme/form/images/img-01.png')}}" alt="IMG">
				</div>
            </div> 
            <div class="col-md-6">     
                 <div class="section-title" >
                        <h3><span style="color:cadetblue !important; margin-right: 5%;">
                                    ¡Te asesoramos,  hablanos de ti! </span></h4>
                 </div>
			     <form action="{{action('SubscriberController@storeWeb')}}" method="POST" class="login100-form validate-form">
					@csrf
					<div class="form-group">
							<label for="name">Nombre y Apellido</label>
							<input type="text" class="form-control input100" name="name" placeholder="Nombre y Apellido" required>
							<div class="valid-feedback">
									Excelente!
							</div>
					</div> 
				 
					<div class="form-group">
							<label for=email>Email</label>
							<input type="email" class="form-control input100" name="email" placeholer="Email" placeholder="Correo eléctronico" required>
							<div class="valid-feedback">
									Excelente!
							</div>
					</div>

					
					<div class="container-login100-form-btn ">
						<button type="submit" class="btn btn-primary btn-lg btn-block">
							Solicitar
						</button>
					</div>
					
                </form>
                    </div>
               </div>     
              
     </div>
     
     

	
