<div class="container">
      <div class="section-title">
                <h3>NUESTROS PLANES</h3>
      </div>

      <div class="card-deck">
                <div class="card pricing">
                    <div class="card-head">
                        <small class="text-primary">EMPRENDEDOR</small>
                        <span class="price"><sub>EMPRENDEDOR</sub></span>
                    </div>
                    <ul class="list-group list-group-flush">
                        <div class="list-group-item">Diseño de Página WEB</div>
                        <div class="list-group-item">Diseño de Logo Vectorizado</div>
                        <div class="list-group-item"><del>Diseño de Banners</del></div>
                        <div class="list-group-item">Diseño de Avatar para RRSS</div>
                        <div class="list-group-item">Video Animado</div>
                        <div class="list-group-item"><del>Diseño de Firma Personal</del></div>
                        <div class="list-group-item">1 año de Hosting + Dominio</div>
                    </ul>
                    <div class="card-footer bg-gradient">
                        &nbsp;
                    </div>
                </div>
                <div class="card pricing popular">
                    <div class="card-head">
                        <small class="text-primary">EL LIDER
                            </small>
                        <span class="price"><sub>LIDER</sub></span>
                    </div>
                    <ul class="list-group list-group-flush">
                            <div class="list-group-item">Branding</div>
                            <div class="list-group-item">Diseño de Página WEB</div>
                            <div class="list-group-item">Diseño de 2 Banners</div>
                            <div class="list-group-item">Diseño de Avatar para RRSS</div>
                            <div class="list-group-item">Diseño de Firma Personal</div>
                            <div class="list-group-item">2 Videos Animados</div>                     
                            <div class="list-group-item">2 años de Hosting+Dominio+SSL</div>
                    </ul>
                    <div class="card-footer bg-gradient">
                            &nbsp;
                        </div>
                    
                </div>
                <div class="card pricing">
                    <div class="card-head">
                        <small class="text-primary">EL DESTACADO</small>
                        <span class="price"><sub>DESTACADO</sub></span>
                    </div>
                    <ul class="list-group list-group-flush">
                        <div class="list-group-item">Branding</div>
                        <div class="list-group-item">Diseño de Página WEB</div>
                        <div class="list-group-item">Diseño de 4 Banners</div>
                        <div class="list-group-item">Diseño de Avatar para RRSS</div>
                        <div class="list-group-item">Diseño de 3 Firmas Personales</div>
                        <div class="list-group-item">5 Videos Animados</div>                     
                        <div class="list-group-item">2 años de Hosting+Dominio+SSL</div>

                    </ul>
                    <div class="card-footer bg-gradient">
                            &nbsp;
                        </div>
                    
                </div>
      </div>

</div>

