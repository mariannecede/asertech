<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   @section('head')
     @include('app/head')
    
    <body>
        <header> 
          <section id="menu">
              @include('app/menu')
          </section> 

          <section id="banner">
              @include('app/header')
          </section>        
      
        </header>

        @yield('page')
        
        <footer class="light-bg py-5" id="contact">
                @include('app/footer')  
        </footer>
   
     @section('script')
       @include('app/script')
    
    </body>
</html>