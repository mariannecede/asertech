  <div class="container">
            

            <ul class="nav nav-tabs nav-justified" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#paginas-web">Páginas Web</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#aplicaciones">Aplicaciones</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#branding">Branding</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#redes-sociales">Redes Sociales</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="paginas-web">
                    <div class="d-flex flex-column flex-lg-row">
                        <img src="{{asset('theme/images/pagina-web.png')}}" alt="paginas-web" class="img-fluid rounded align-self-start mr-lg-5 mb-5 mb-lg-0">
                        <div>

                            <h2>Creamos tu imagen digital</h2>
                            <p class="lead"> Wordpress, Shopify, Magento, HTLM, CCS </p>
                            <p>Te guiamos a seleccionar la mejor herramienta para elaborar tu página, 
                                cumpliendo con las necesidades y exigencias que requiere tu modelo de negocios.</p>
                            <p> Contamos con todas las herramientas y stándares del mercado web, 
                                que nos permitan la materialización de tu idea, acentuando la innovación y excelencia en cada proceso del proyecto.</p>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="applicaciones">
                    <div class="d-flex flex-column flex-lg-row">
                        <div>
                            <h2>Desarrollamos tu negocio</h2>
                            <p class="lead"> Laravel, Django, Express.js, Angular, Vue, React</p>
                            <p>Sabemos la importancia y responsabilidad que exige gestionar un negocio.</p> 
                            <p> Por ello, en Asertech buscamos de automatizar todos los procesos tanto 
                                administrativos como operativos que existen en el flujo del mismo. </p>
                            <p> Te brindamos el mejor equipo de desarrollo que hemos destinado a que haga de tus ideas, un producto. 
                            </p>
                        </div>
                        <img src="{{asset('theme/images/programacion.png')}}" style="width:27%;" alt="aplicaciones-web" class="img-fluid rounded align-self-start mr-lg-5 mb-5 mb-lg-0">
                    </div>
                </div>
                <div class="tab-pane fade" id="branding">
                    <div class="d-flex flex-column flex-lg-row">
                        <img src="theme/images/branding.png" style="width:20%;" alt="branding-logos" class="img-fluid rounded align-self-start mr-lg-5 mb-5 mb-lg-0">
                        <div>
                            <h2>Nos identificamos con tu marca</h2>
                            <p class="lead">Adobe Photoshop, Adobe illustrator, After Effects, InDesign</p>
                            <p>Cada diseño elaborado, debe estar plenamente identificado con los 
                                valores y personalidad de tu marca.</p>
                            <p> Nos encargamos de transmitirlo en cada elemento contemplado en la pieza final.</p>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="redes-sociales">
                    <div class="d-flex flex-column flex-lg-row">
                        <div>
                            <h2>Gestionamos tu comunicación</h2>
                            <p class="lead">Facebook, Instagram, Twitter, Youtube, Linkedin </p>
                            <p>El valor más preciado de un negocio es el cliente.</p>
                            <p>Con la cercanía que nos permiten las redes sociales, 
                                    en Asertech creamos todas las estrategias de conversión ideales
                                    para lograr la fidelización de tu publico objetivo.

                            </p>
                            <p>En la actualidad, una interacción, es la base principal para la comunicación. </p>
 
                        </div>
                        <img src="{{asset('theme/images/redes-sociales.png')}}" alt="redes-sociales" class="img-fluid rounded align-self-start mr-lg-5 mb-5 mb-lg-0">
                    </div>
                </div>
            </div>


  </div>
