<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

 @section('head')
  @include('app/head')

 <body data-spy="scroll" data-target="#navbar" data-offset="30">
       
    <header>
        <section class="section bg-gradient" style="padding: 27px 0 !important;background: white;">    
       
          @include('pages-send-form/headband-one')
      
        </section>    
    </header>    

<footer class="light-bg py-5" id="contact">
        @include('app/footer')
    </footer> 

    @section('script')
       @include('app/script')
 </body>
</html>

<style>
    header{
        padding: 0px 0 0 !important; 
        text-align: center;
    
    }
        
</style>    