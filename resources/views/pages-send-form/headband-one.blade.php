        <div class="card mb-3 border-0" style="max-width: 540px;display: inline-table">
                
                <img src="{{asset('theme/images/presupuesto.png')}}" class="card-img-top" alt="...">
                <div class="card-body">
                  <h5 class="card-title">¡Gracias por confiar en Nosotros!</h5>
                  <p class="card-text">Su información ha llegado con éxito a nuestro sistema.</p>
                  <p> Estaremos atendiendo su solicitud y nos pondremos en 
                      contacto con usted. </p></br>
                   <div> <a href="{{ url('/') }}" class="btn btn-primary">Regresar</a>  </div>
                  
                </div>
              </div>
