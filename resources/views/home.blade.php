@extends('app')

@section('page')  
 <body data-spy="scroll" data-target="#navbar" data-offset="30">
       
    <section id="my-characteristics" class="section light-bg-two">
       @include('characteristics/my-characteristics')
    </section>
 
    <section id="my-services" class="section light-bg">
       @include('services/my-services')
    </section>

    <section id="advisory" class="section light-bg-two">
       @include('advisory/form')
    </section>

    <section id="my-plans" class="section light-bg">
       @include('plans/my-plans')
    </section>

    <section id="budget" class="section light-bg-two">
       @include('budget/budget')
    </section>

 </body>
@endsection

