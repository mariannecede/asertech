   <div class="light-bg py-5" id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 text-center text-lg-left">
                    <p class="mb-2"> <span class="ti-location-pin mr-2"></span> Av. Francisco de Miranda, Edif Roraima, Piso 14, Ofic 14F</p>
                    <div class=" d-block d-sm-inline-block">
                        <p class="mb-2">
                            <span class="ti-email mr-2"></span> <a class="mr-4" href="mailto:cotizaciones@asertech.io">cotizaciones@asertech.io</a>
                        </p>
                    </div>
                    <div class="d-block d-sm-inline-block">
                        <p class="mb-0">
                            <span class="ti-headphone-alt mr-2"></span> <a href="tel:584128278961">58 412-8278961</a>
                        </p>
                    </div>

                </div>
                <div class="col-lg-6">
                    <div class="social-icons">
                        <a href="https://www.facebook.com/asertech1/"><span class="ti-facebook"></span></a>
                        <a href="https://twitter.com/asertech_"><span class="ti-twitter-alt"></span></a>
                        <a href="https://www.instagram.com/aser.tech/"><span class="ti-instagram"></span></a>
                    </div>
                </div>
            </div>

        </div>

    </div>
 
