<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" >
<body data-spy="scroll" data-offset="30">
   
<header class=""  id="home">
     <section>
            <div class="text-white">
                    <img src="{{ asset('theme/images/banner-email.jpg')}}"; class="card-img" alt="correo promoción">
                    
             </div>
            
     </section>
</header> 

<section id="content-email" class="row justify-content-md-center">
   @include('email/content-email')
</section>


<footer class="light-bg py-5" id="">
    @include('footer')
</footer>

@section('script')
   @include('script')
</body>
</html>


