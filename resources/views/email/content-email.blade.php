<div class="row">
		<div class="col-sm-6">
			<div class="card">
				<div class="card-body">
						<h5 class="card-title" style="font-weight: 400; margin-top: 16px;">Activate con ASERTECH</h5>
										<p class="texto-email"> Desde éste momento, te encuentras participando 
											 en el sorteo que realizaremos el día 19 de Agosto de 2019, 
											 con motivo de nuestro: </p>
											 <p><span style="color: #633991; font-weight:500;padding: 24%;"> "Gran Lanzamiento WEB."</span></p>
											 <p class="texto-email">El ganador tendrá la oportunidad de obtener una Página Web Básica 
											 con todos los estándares que exige el mercado.<p>
											 <p class="texto-email">En AserTech seguimos apoyando el emprendimiento y el impulso al camino de la digitalización empresarial.<br>
	
											
														<img src="{{asset('theme/images/Computer.png')}}" class="img-thumbnail" 
														style="max-width: 45% !important; padding: 2.25rem !important;border: 0px solid #ddd !important;" alt="promoción de pagina web">
										
												<a href="https://asertech.io/" class="btn btn-primary">Materializamos tus ideas</a>
	
										 <p class="card-text"><small class="text-muted">Promoción Válida hasta el 18/08/2019</small></p> 
				</div>
			</div>
		</div>
</div>		   


    